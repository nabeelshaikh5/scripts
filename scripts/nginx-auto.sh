#An nginx installer with also a sample page auto deployed

#I am going to use nginx.io where they compile and give the latest version of nginx

#Installer start

#Installing dependencies
sudo apt install -y lsb-release ca-certificates wget gnupg unzip -y

#Adding the keyring 
wget -O /usr/share/keyrings/nginx-io.asc https://nginx.io/public.key

#Adding the apt repository
sudo echo "deb [signed-by=/usr/share/keyrings/nginx-io.asc] http://nginx-io.mirror.cdnweb.network/nginx/ $(lsb_release -sc) main" > /etc/apt/sources.list.d/nginx-io.list

#Updating apt to include the newely added repository
sudo apt update

#Installing it
sudo apt install -y nginx-extras

#Installation end

#Getting a template from Html5up.net and putting it in html folder
cd /var/www/html
sudo wget https://static.cdnweb.network/files/html5up-story.zip
sudo unzip html5up-story.zip
#end of downloading the template
